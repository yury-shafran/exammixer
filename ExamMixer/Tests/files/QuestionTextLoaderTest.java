package files;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class QuestionTextLoaderTest implements QuestionTextLoader.Storage, IdRecognizer {

    private final Map<String, Map<String, Collection<String>>> storage = new HashMap<>();
    private final QuestionTextLoader questionTextLoader = new QuestionTextLoader(this, this);
    private final TextLoader textLoader = new TextLoader(questionTextLoader);

    @Override
    public void addOptionText(String questionId, String optionId, List<String> text) {
        storage.putIfAbsent(questionId, new HashMap<>());
        storage.get(questionId).putIfAbsent(optionId, new LinkedList<>());
        storage.get(questionId).get(optionId).addAll(text);
    }

    private static final String questionIdGroup = "questionId", optionIdGroup = "optionId";
    private static final String idsRegex = "(?<" + questionIdGroup + ">\\w+?)-(?<" + optionIdGroup + ">\\w+).*";
    private static final Pattern idsPattern = Pattern.compile(idsRegex);

    @Override
    public Ids recognize(String text) {
        Ids ids = null;
        Matcher matcher = idsPattern.matcher(text);
        if (matcher.matches()) {
            String questionId = matcher.group(questionIdGroup);
            String optionId = matcher.group(optionIdGroup);
            ids = new Ids(questionId, optionId);
        }
        return ids;
    }

    private void load(String input) throws IOException, TextLoader.TextFormatException {
        StringReader stringReader = new StringReader(input);
        textLoader.load(new BufferedReader(stringReader));
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void load() throws IOException, TextLoader.TextFormatException {
        String input = "Alpha-2\nQuestion text\nAnother line\n" +
                "Alpha-5d\nTeeext!\n" +
                "Alpha-default\nDefault text\n" +
                "Gamma-A // Comment here.\nThe question.\n" +
                "Alpha-2\nAddition!\n";
        load(input);
        Collection<String> alpha2 = List.of("Question text", "Another line", "Addition!");
        assertEquals(alpha2, storage.get("Alpha").get("2"));
        assertEquals(List.of("Teeext!"), storage.get("Alpha").get("5d"));
        assertEquals(List.of("Default text"), storage.get("Alpha").get("default"));
        assertEquals(List.of("The question."), storage.get("Gamma").get("A"));
    }

}