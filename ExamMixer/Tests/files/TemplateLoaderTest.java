package files;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class TemplateLoaderTest implements TemplateLoader.Storage, TemplateLoader.TemplateRecognizer {


    private static final String beginTemplateIdGroup = "beginTemplateId";
    private static final String endTemplateIdGroup = "endTemplateId";
    private static final String randomIdGroup = "randomId";
    private static final String[] literalKeywords = {"EMGenerate", "EMIndex", "EMImagePath"};
    private static final String regex;
    private static final Pattern pattern;

    static {
        String beginTemplateRegex = "^EMTemplate-Begin-(?<" + beginTemplateIdGroup + ">\\w+).*$";
        String endTemplateRegex = "^EMTemplate-End-(?<" + endTemplateIdGroup + ">\\w+).*$";
        String randomRegex = "EMRandom-(?<" + randomIdGroup + ">\\w+)";
        Stream<String> regs = Stream.of(beginTemplateRegex, endTemplateRegex, randomRegex);
        regs = Stream.concat(regs, Stream.of(literalKeywords));
        regex = regs.collect(Collectors.joining("|"));
        pattern = Pattern.compile(regex);
    }


    @Override
    public Matcher match(String line) {
        return pattern.matcher(line);
    }

    @Override
    public String beginTemplateId(Matcher matcher) {
        return matcher.group(beginTemplateIdGroup);
    }

    @Override
    public String endTemplateId(Matcher matcher) {
        return matcher.group(endTemplateIdGroup);
    }

    @Override
    public String keyword(Matcher matcher) {
        String keyword = matcher.group(randomIdGroup);
        if (Objects.isNull(keyword)) {
            keyword = matcher.group();
        }
        return keyword;
    }

    private final String documentId = "Document";
    private final Map<String, List<Map.Entry<String, Boolean>>> templates = new HashMap<>();
    @SuppressWarnings("FieldCanBeLocal")
    private TemplateLoader templateLoader;
    private TextLoader textLoader;

    @Override
    public void add(String templateId, String line, boolean literal) {
        templates.putIfAbsent(templateId, new LinkedList<>());
        templates.get(templateId).add(Map.entry(line, literal));
    }

    @Before
    public void setUp() {
        templateLoader = new TemplateLoader(this, this, documentId);
        textLoader = new TextLoader(templateLoader);
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void load() throws IOException, TextLoader.TextFormatException {
        List<Map.Entry<String, Boolean>> expectedDocumentTemplate = List.of(
                Map.entry("<!DOCTYPE html>", true),
                Map.entry("<html lang=\"en-US\">", true),
                Map.entry("<head>", true),
                Map.entry("<meta charset=\"UTF-8\">", true),
                Map.entry("<!--  -->", true),
                Map.entry("<style>", true),
                Map.entry("body\t\t{font-family:times new roman;}", true),
                Map.entry("li.text\t\t{text-align:justify;}", true),
                Map.entry("</style>", true),
                Map.entry("</head>", true),
                Map.entry("<body>", true),
                Map.entry("EMGenerate", false),
                Map.entry("</body>", true),
                Map.entry("</html>", true)
        );
        List<Map.Entry<String, Boolean>> expectedPaperTemplate = List.of(
                Map.entry("<article class=\"card\">", true),
                Map.entry("<table>", true),
                Map.entry("\t<tr>", true),
                Map.entry("\t\t<td class=\"alignright\">Subject:</td>", true),
                Map.entry("\t\t<td>Java (2016-2017)</td>", true),
                Map.entry("\t</tr>", true),
                Map.entry("</table>", true),
                Map.entry("<p class=\"center\">Paper # ", true),
                Map.entry("EMIndex", false),
                Map.entry("</p>", true),
                Map.entry("<ol type=\"1\">", true),
                Map.entry("\t<li class=\"text\">", true),
                Map.entry("Base", false),
                Map.entry("</li>", true),
                Map.entry("\t<li class=\"text\">", true),
                Map.entry("Classes", false),
                Map.entry("</li>", true),
                Map.entry("\t<li class=\"text\">", true),
                Map.entry("Streams", false),
                Map.entry("</li>", true),
                Map.entry("\t<li class=\"text\">", true),
                Map.entry("Practice", false),
                Map.entry("</li>", true),
                Map.entry("</ol>", true),
                Map.entry("<table class=\"full\">", true),
                Map.entry("</table>", true),
                Map.entry("</article>", true)
        );
        List<Map.Entry<String, Boolean>> expectedImageTemplate = List.of(
                Map.entry("<p><img src=\"", true),
                Map.entry("EMImagePath", false),
                Map.entry("\"/></p>", true)
        );
        String input = "<!DOCTYPE html>\n" +
                "<html lang=\"en-US\">\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<!--  -->\n" +
                "<style>\n" +
                "body\t\t{font-family:times new roman;}\n" +
                "li.text\t\t{text-align:justify;}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "EMGenerate\n" +
                "\n" +
                "EMTemplate-Begin-Paper\n" +
                "<article class=\"card\">\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<td class=\"alignright\">Subject:</td>\n" +
                "\t\t<td>Java (2016-2017)</td>\n" +
                "\t</tr>\n" +
                "</table>\n" +
                "\n" +
                "<p class=\"center\">Paper # EMIndex</p>\n" +
                "<ol type=\"1\">\n" +
                "\t<li class=\"text\">EMRandom-Base</li>\n" +
                "\t<li class=\"text\">EMRandom-Classes</li>\n" +
                "\t<li class=\"text\">EMRandom-Streams</li>\n" +
                "\t<li class=\"text\">EMRandom-Practice</li>\n" +
                "</ol>\n" +
                "<table class=\"full\">\n" +
                "</table>\n" +
                "</article>\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "EMTemplate-Begin-Image\n" +
                "<p><img src=\"EMImagePath\"/></p>\n" +
                "EMTemplate-End-Image\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        textLoader.load(new BufferedReader(new StringReader(input)));
        assertEquals(expectedDocumentTemplate, templates.get(documentId));
        assertEquals(expectedPaperTemplate, templates.get("Paper"));
        assertEquals(expectedImageTemplate, templates.get("Image"));
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test(expected = TemplateLoader.UnexpectedTemplateEndException.class)
    public void anotherEnd() throws IOException, TextLoader.TextFormatException {
        String input = "<!DOCTYPE html>\n" +
                "<html lang=\"en-US\">\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<!--  -->\n" +
                "<style>\n" +
                "body\t\t{font-family:times new roman;}\n" +
                "li.text\t\t{text-align:justify;}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "EMGenerate\n" +
                "\n" +
                "EMTemplate-Begin-Paper\n" +
                "<article class=\"card\">\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<td class=\"alignright\">Subject:</td>\n" +
                "\t\t<td>Java (2016-2017)</td>\n" +
                "\t</tr>\n" +
                "</table>\n" +
                "\n" +
                "<p class=\"center\">Paper # EMIndex</p>\n" +
                "<ol type=\"1\">\n" +
                "\t<li class=\"text\">EMRandom-Base</li>\n" +
                "\t<li class=\"text\">EMRandom-Classes</li>\n" +
                "\t<li class=\"text\">EMRandom-Streams</li>\n" +
                "\t<li class=\"text\">EMRandom-Practice</li>\n" +
                "</ol>\n" +
                "<table class=\"full\">\n" +
                "</table>\n" +
                "</article>\n" +
                "EMTemplate-End-Image\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "EMTemplate-Begin-Image\n" +
                "<p><img src=\"EMImagePath\"/></p>\n" +
                "EMTemplate-End-Image\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        textLoader.load(new BufferedReader(new StringReader(input)));
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test(expected = TemplateLoader.UnexpectedTemplateEndException.class)
    public void endOfNothing() throws IOException, TextLoader.TextFormatException {
        String input = "<!DOCTYPE html>\n" +
                "<html lang=\"en-US\">\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<!--  -->\n" +
                "<style>\n" +
                "body\t\t{font-family:times new roman;}\n" +
                "li.text\t\t{text-align:justify;}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "EMGenerate\n" +
                "EMTemplate-End-Document\n" +
                "\n" +
                "EMTemplate-End-Image\n" +
                "EMTemplate-Begin-Paper\n" +
                "<article class=\"card\">\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<td class=\"alignright\">Subject:</td>\n" +
                "\t\t<td>Java (2016-2017)</td>\n" +
                "\t</tr>\n" +
                "</table>\n" +
                "\n" +
                "<p class=\"center\">Paper # EMIndex</p>\n" +
                "<ol type=\"1\">\n" +
                "\t<li class=\"text\">EMRandom-Base</li>\n" +
                "\t<li class=\"text\">EMRandom-Classes</li>\n" +
                "\t<li class=\"text\">EMRandom-Streams</li>\n" +
                "\t<li class=\"text\">EMRandom-Practice</li>\n" +
                "</ol>\n" +
                "<table class=\"full\">\n" +
                "</table>\n" +
                "</article>\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "EMTemplate-Begin-Image\n" +
                "<p><img src=\"EMImagePath\"/></p>\n" +
                "EMTemplate-End-Image\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        textLoader.load(new BufferedReader(new StringReader(input)));
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test(expected = TemplateLoader.IncompleteTemplateException.class)
    public void noEnd() throws IOException, TextLoader.TextFormatException {
        String input = "<!DOCTYPE html>\n" +
                "<html lang=\"en-US\">\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<!--  -->\n" +
                "<style>\n" +
                "body\t\t{font-family:times new roman;}\n" +
                "li.text\t\t{text-align:justify;}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "EMGenerate\n" +
                "\n" +
                "EMTemplate-Begin-Paper\n" +
                "<article class=\"card\">\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<td class=\"alignright\">Subject:</td>\n" +
                "\t\t<td>Java (2016-2017)</td>\n" +
                "\t</tr>\n" +
                "</table>\n" +
                "\n" +
                "<p class=\"center\">Paper # EMIndex</p>\n" +
                "<ol type=\"1\">\n" +
                "\t<li class=\"text\">EMRandom-Base</li>\n" +
                "\t<li class=\"text\">EMRandom-Classes</li>\n" +
                "\t<li class=\"text\">EMRandom-Streams</li>\n" +
                "\t<li class=\"text\">EMRandom-Practice</li>\n" +
                "</ol>\n" +
                "<table class=\"full\">\n" +
                "</table>\n" +
                "</article>\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "EMTemplate-Begin-Image\n" +
                "<p><img src=\"EMImagePath\"/></p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        textLoader.load(new BufferedReader(new StringReader(input)));
    }

}