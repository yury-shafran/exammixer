package files;

import org.junit.Test;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QuestionImageLoaderTest implements QuestionImageLoader.Storage, IdRecognizer {

    private final Map<String, Map<String, Collection<String>>> storage = new HashMap<>();
    private final QuestionImageLoader questionImageLoader = new QuestionImageLoader(this, this);

    @Override
    public void addOptionImage(String questionId, String optionId, String image) {
        storage.putIfAbsent(questionId, new HashMap<>());
        storage.get(questionId).putIfAbsent(optionId, new LinkedList<>());
        storage.get(questionId).get(optionId).add(image);
    }

    private static final String questionIdGroup = "questionId", optionIdGroup = "optionId";
    private static final String idsRegex = "(?<" + questionIdGroup + ">\\w+?)-(?<" + optionIdGroup + ">\\w+).*";
    private static final String idsRegexWeak = ".*?" + idsRegex;
    private static final Pattern idsPatternWeak = Pattern.compile(idsRegexWeak);

    @Override
    public Ids recognize(String text) {
        Ids ids = null;
        Matcher matcher = idsPatternWeak.matcher(text);
        if (matcher.matches()) {
            String questionId = matcher.group(questionIdGroup);
            String optionId = matcher.group(optionIdGroup);
            ids = new Ids(questionId, optionId);
        }
        return ids;
    }

    private void load(File... input) {
        questionImageLoader.load(List.of(input));
    }

    @Test
    public void load() {
        load(
                new File("C:\\Image for Alpha-23.jpeg"),
                new File("C:\\q Alpha-5d.bmp"),
                new File("C:\\q-12\\"),
                new File("C:\\A-1\\Alpha-23.bmp"),
                new File("C:\\first\\Folder-A\\Alpha-8.gif"),
                new File("C:\\Gamma-12 (bad).jpg")
        );
        assertEquals(List.of("C:\\Image for Alpha-23.jpeg", "C:\\A-1\\Alpha-23.bmp"), storage.get("Alpha").get("23"));
        assertEquals(List.of("C:\\q Alpha-5d.bmp"), storage.get("Alpha").get("5d"));
        assertEquals(List.of("C:\\first\\Folder-A\\Alpha-8.gif"), storage.get("Alpha").get("8"));
        assertEquals(List.of("C:\\Gamma-12 (bad).jpg"), storage.get("Gamma").get("12"));
        assertNull(storage.get("A"));
        assertNull(storage.get("Folder"));
    }

}