package sources;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DocumentBuilderTest {

    private final DocumentBuilder documentBuilder = new DocumentBuilder();

    @Test(expected = NullPointerException.class)
    public void nullLiteral() {
        documentBuilder.addLiteral("ASD", null, "123");
    }

    @Test
    public void generation() throws Exception {
        Collection<String> start = List.of("First line.", "Second line.");
        Collection<String> middle1 = List.of("M1");
        Collection<String> middle2 = List.of("1", "2", "3");
        Collection<String> finish = List.of("The end.", "Nothing else.", "Leave it.");
        documentBuilder.addLiteral(start.toArray(new String[0]));
        documentBuilder.addContent("id2");
        documentBuilder.addLiteral(middle1.toArray(new String[0]));
        documentBuilder.addContent("id1");
        documentBuilder.addLiteral(middle2.toArray(new String[0]));
        documentBuilder.addContent("id2");
        documentBuilder.addLiteral(finish.toArray(new String[0]));

        List<String> id1Content = List.of();
        List<String> id2Content = List.of("Hello, world!", "One line.\nAnd another.");
        DocumentBuilder.ContentProvider provider = (index, contentId, writer) -> {
            switch (contentId) {
                case "id1":
                    id1Content.forEach(writer::println);
                    break;
                case "id2":
                    id2Content.forEach(writer::println);
                    break;
                default:
                    throw new Exception();
            }
        };

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        documentBuilder.build(1, provider, printWriter);

        StringWriter expectedStringWriter = new StringWriter();
        PrintWriter expectedPrintWriter = new PrintWriter(expectedStringWriter);
        List<String> expected = new LinkedList<>(start);
        expected.addAll(id2Content);
        expected.addAll(middle1);
        expected.addAll(id1Content);
        expected.addAll(middle2);
        expected.addAll(id2Content);
        expected.addAll(finish);
        expected.forEach(expectedPrintWriter::println);

        assertEquals(expectedStringWriter.toString(), stringWriter.toString());
    }

}