package sources;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class RandomOptionSupplierTest {

    private final Map<Integer, Integer> counts =
            new HashMap<>(Map.of(2, 0, 3, 0, 5, 0));
    private RandomOptionSupplier<Integer> randomOptionSupplier;

    @Before
    public void setUp() {
        randomOptionSupplier = new RandomOptionSupplier<>(counts.keySet());
    }

    @Test
    public void evenness() {
        int laps = 3;
        for (int i = 0; i < counts.size() * laps; i++) {
            Integer variant = randomOptionSupplier.get();
            counts.put(variant, counts.get(variant) + 1);
        }
        for (Integer value : counts.values()) {
            assertEquals(laps, (int) value);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyCollection() {
        randomOptionSupplier = new RandomOptionSupplier<>(Set.of());
    }

}