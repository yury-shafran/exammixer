package sources;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RandomOptionSuppliersTest {

    private final Map<Integer, Map<Integer, Integer>> testMap = Map.of(
            0, new HashMap<>(Map.of(2, 0, 3, 0, 5, 0)),
            1, new HashMap<>(Map.of(0, 0, 1, 0)),
            5, new HashMap<>(Map.of(5, 0, 6, 0, 9, 0, 8, 0))
    );
    private RandomOptionSuppliers<Integer, Integer> randomOptionSuppliers;

    @Before
    public void setUp() {
        RandomOptionSuppliers.Builder builder = RandomOptionSuppliers.builder();
        for (Integer number : testMap.keySet()) {
            //noinspection unchecked
            builder.add(number, testMap.get(number).keySet());
        }
        //noinspection unchecked
        randomOptionSuppliers = builder.build();
    }

    @Test
    public void evenness() {
        int count = 3;
        for (int k = 0; k < count; k++) {
            for (Integer questionId : testMap.keySet()) {
                Map<Integer, Integer> optionsMap = testMap.get(questionId);
                for (int i = 0; i < optionsMap.size(); i++) {
                    Integer option = randomOptionSuppliers.get(questionId);
                    optionsMap.put(option, optionsMap.get(option) + 1);
                }
            }
        }
        for (Integer questionId : testMap.keySet()) {
            Map<Integer, Integer> optionsMap = testMap.get(questionId);
            for (Integer option : optionsMap.keySet()) {
                assertEquals(count, (int) optionsMap.get(option));
            }
        }
    }

    @Test(expected = NullPointerException.class)
    public void wrongNumber() {
        randomOptionSuppliers.get(2);
    }

}