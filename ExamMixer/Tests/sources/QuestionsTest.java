package sources;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class QuestionsTest {

    @SuppressWarnings("FieldCanBeLocal")
    private final String defaultOptionId = "DEF";
    private final Questions questions = new Questions();
    private final Map<String, Map<String, List<String>>> text, images, resultText, resultImages;
    @SuppressWarnings("FieldCanBeLocal")
    private final String defaultImage = "i" + defaultOptionId;

    {
        text = Map.of(
                "A", Map.of( // No default
                        "1", List.of("A1-1", "A1-2", "A1-3"),
                        "2", List.of("A2-1"),
                        "3", List.of("A3-1")
                ),
                "B", Map.of( // Default first
                        defaultOptionId, List.of(defaultOptionId),
                        "1", List.of("B1-1", "B1-2", "B1-3"),
                        "2", List.of("B2-1"),
                        "3", List.of("B3-1")
                ),
                "C", Map.of( // Default in the middle
                        "1", List.of("C1-1", "C1-2", "C1-3"),
                        defaultOptionId, List.of(defaultOptionId),
                        "2", List.of("C2-1"),
                        "3", List.of("C3-1")
                ),
                "G", Map.of( // Default last
                        "1", List.of("G1-1", "G1-2", "G1-3"),
                        "2", List.of("G2-1"),
                        "3", List.of("G3-1"),
                        defaultOptionId, List.of(defaultOptionId)
                ),
                "H", Map.of( // Single option (only text)
                        "O", List.of("HO-1", "HO-2", "HO-3")
                ),
                "J", Map.of( // Single option
                        "O", List.of("JO-1", "JO-2", "JO-3")
                ),
                "K", Map.of( // Single default option (only text)
                        defaultOptionId, List.of(defaultOptionId + "-1", defaultOptionId + "-2")
                ),
                "Q", Map.of( // Single default option
                        defaultOptionId, List.of(defaultOptionId + "-1", defaultOptionId + "-2")
                )
        );
        resultText = Map.of(
                "A", Map.of( // No default
                        "1", List.of("A1-1", "A1-2", "A1-3"),
                        "2", List.of("A2-1"),
                        "3", List.of("A3-1")
                ),
                "B", Map.of( // Default first
                        defaultOptionId, List.of(defaultOptionId),
                        "1", List.of(defaultOptionId, "B1-1", "B1-2", "B1-3"),
                        "2", List.of(defaultOptionId, "B2-1"),
                        "3", List.of(defaultOptionId, "B3-1")
                ),
                "C", Map.of( // Default in the middle
                        "1", List.of(defaultOptionId, "C1-1", "C1-2", "C1-3"),
                        defaultOptionId, List.of(defaultOptionId),
                        "2", List.of(defaultOptionId, "C2-1"),
                        "3", List.of(defaultOptionId, "C3-1")
                ),
                "G", Map.of( // Default last
                        "1", List.of(defaultOptionId, "G1-1", "G1-2", "G1-3"),
                        "2", List.of(defaultOptionId, "G2-1"),
                        "3", List.of(defaultOptionId, "G3-1"),
                        defaultOptionId, List.of(defaultOptionId)
                ),
                "H", Map.of( // Single option (only text)
                        "O", List.of("HO-1", "HO-2", "HO-3")
                ),
                "J", Map.of( // Single option
                        "O", List.of("JO-1", "JO-2", "JO-3")
                ),
                "K", Map.of( // Single default option (only text)
                        defaultOptionId, List.of(defaultOptionId + "-1", defaultOptionId + "-2")
                ),
                "Q", Map.of( // Single default option
                        defaultOptionId, List.of(defaultOptionId + "-1", defaultOptionId + "-2")
                )
        );
        images = Map.of(
                "A", Map.of(
                        "1", List.of("iA1-1", "iA1-2", "iA1-3"), // Text and images
                        // "2" // Only text
                        "4", List.of("iA4-1") // Only images
                ),
                "B", Map.of( // Default first
                        defaultOptionId, List.of(defaultImage),
                        "1", List.of("iB1-1", "iB1-2", "iB1-3"),
                        "2", List.of("iB2-1"),
                        "3", List.of("iB3-1")
                ),
                "C", Map.of( // Default in the middle
                        "1", List.of("iC1-1", "iC1-2", "iC1-3"),
                        defaultOptionId, List.of(defaultImage),
                        "2", List.of("iC2-1"),
                        "3", List.of("iC3-1")
                ),
                "G", Map.of( // Default last
                        "1", List.of("iG1-1", "iG1-2", "iG1-3"),
                        "2", List.of("iG2-1"),
                        "3", List.of("iG3-1"),
                        defaultOptionId, List.of(defaultImage)
                ),
                "J", Map.of( // Single option
                        "O", List.of("iJO-1", "iJO-2", "iJO-3")
                ),
                "Q", Map.of( // Single default option
                        defaultOptionId, List.of(defaultImage + "-1", defaultImage + "-2")
                ),
                "R", Map.of( // Single option (only image)
                        "O", List.of("iHO-1", "iHO-2", "iHO-3")
                ),
                "S", Map.of( // Single default option (only image)
                        defaultOptionId, List.of(defaultImage + "-1", defaultImage + "-2")
                )
        );
        resultImages = Map.of(
                "A", Map.of(
                        "1", List.of("iA1-1", "iA1-2", "iA1-3"), // Text and images
                        // "2" // Only text
                        "4", List.of("iA4-1") // Only images
                ),
                "B", Map.of( // Default first
                        defaultOptionId, List.of(defaultImage),
                        "1", List.of(defaultImage, "iB1-1", "iB1-2", "iB1-3"),
                        "2", List.of(defaultImage, "iB2-1"),
                        "3", List.of(defaultImage, "iB3-1")
                ),
                "C", Map.of( // Default in the middle
                        "1", List.of(defaultImage, "iC1-1", "iC1-2", "iC1-3"),
                        defaultOptionId, List.of(defaultImage),
                        "2", List.of(defaultImage, "iC2-1"),
                        "3", List.of(defaultImage, "iC3-1")
                ),
                "G", Map.of( // Default last
                        "1", List.of(defaultImage, "iG1-1", "iG1-2", "iG1-3"),
                        "2", List.of(defaultImage, "iG2-1"),
                        "3", List.of(defaultImage, "iG3-1"),
                        defaultOptionId, List.of(defaultImage)
                ),
                "J", Map.of( // Single option
                        "O", List.of("iJO-1", "iJO-2", "iJO-3")
                ),
                "Q", Map.of( // Single default option
                        defaultOptionId, List.of(defaultImage + "-1", defaultImage + "-2")
                ),
                "R", Map.of( // Single option (only image)
                        "O", List.of("iHO-1", "iHO-2", "iHO-3")
                ),
                "S", Map.of( // Single default option (only image)
                        defaultOptionId, List.of(defaultImage + "-1", defaultImage + "-2")
                )
        );
    }

    @Before
    public void setUp() {
        questions.setDefaultOptionId(defaultOptionId);
    }

    private void check() {
        resultText.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionText) ->
                        {
                            try {
                                assertEquals(optionText, questions.getText(questionId, optionId));
                            } catch (Questions.QuestionNotFoundException e) {
                                e.printStackTrace();
                            }
                        }));
        resultImages.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionImages) ->
                        {
                            try {
                                assertEquals(optionImages, questions.getImages(questionId, optionId));
                            } catch (Questions.QuestionNotFoundException e) {
                                e.printStackTrace();
                            }
                        }));
    }

    @Test
    public void addOne() {
        text.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionText) -> optionText.forEach(
                                line -> questions.addOptionText(questionId, optionId, line))));
        images.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionImages) -> optionImages.forEach(
                                image -> questions.addOptionImage(questionId, optionId, image))));
        check();
    }

    @Test
    public void addBulk() {
        text.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionText) ->
                                questions.addOptionText(questionId, optionId, optionText)));
        images.forEach(
                (questionId, optionMap) ->
                        optionMap.forEach((optionId, optionImages) ->
                                questions.addOptionImages(questionId, optionId, optionImages)));
        check();
    }

    @Test
    public void addMoreText() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.addOptionText("B", "2", "B2-2");
        List<String> result = new ArrayList<>(resultText.get("B").get("2"));
        result.add("B2-2");
        assertEquals(result, questions.getText("B", "2"));
    }

    @Test
    public void addMoreTextBulk() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.addOptionText("B", "2", List.of("B2-2", "B2-3"));
        List<String> result = new ArrayList<>(resultText.get("B").get("2"));
        Collections.addAll(result, "B2-2", "B2-3");
        assertEquals(result, questions.getText("B", "2"));
    }

    /*
    "B", Map.of( // Default first
                        defaultOptionId, List.of(defaultImage),
                        "1", List.of("iB1-1", "iB1-2", "iB1-3"),
                        "2", List.of("iB2-1"),
                        "3", List.of("iB3-1")
                )
     */

    @Test
    public void addMoreImages() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.addOptionImage("B", "2", "iB2-2");
        List<String> result = new ArrayList<>(resultImages.get("B").get("2"));
        result.add("iB2-2");
        assertEquals(result, questions.getImages("B", "2"));
    }

    @Test
    public void addMoreImagesBulk() {
        addBulk();
    }

    @Test(expected = Questions.QuestionNotFoundException.class)
    public void getUnknownQuestionText() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.getText("Z", "1");
    }

    @Test(expected = Questions.QuestionNotFoundException.class)
    public void getUnknownQuestionImages() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.getImages("Z", "1");
    }

    @Test(expected = Questions.QuestionNotFoundException.class)
    public void getUnknownOptionText() throws Questions.QuestionNotFoundException {
        addBulk();
        questions.getText("A", "0");
    }

    @Test
    public void getUnknownOptionImages() throws Questions.QuestionNotFoundException {
        addBulk();
        assertEquals(0, questions.getImages("A", "0").size());
    }

    @Test
    public void getOptionWithoutText() throws Questions.QuestionNotFoundException {
        addBulk();
        assertEquals(0, questions.getText("R", "O").size());
    }

    @Test
    public void getOptionWithoutImages() throws Questions.QuestionNotFoundException {
        addBulk();
        assertEquals(0, questions.getImages("H", "O").size());
    }

}