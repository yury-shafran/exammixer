package resources;

import org.junit.Before;
import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

public class ResourceProviderTest {

    @Before
    public void clearResourceCache() {
        ResourceBundle.clearCache();
    }

    @Test
    public void getValueStatic() {
        String testLabel = ResourceProvider.getValue(
                "resources.Labels", Locale.ROOT, "TestLabel", "0");
        assertEquals("Test Label", testLabel);
    }

    @Test
    public void getValueStaticRu() {
        String testLabel = ResourceProvider.getValue(
                "resources.Labels", Locale.getDefault(), "TestLabel", "0");
        assertEquals("Тестовое сообщение", testLabel);
    }

    @Test
    public void getValueObject() {
        ResourceProvider provider = new ResourceProvider("resources.Labels", Locale.ROOT);
        String testLabel = provider.getValue("TestLabel", "0");
        assertEquals("Test Label", testLabel);
    }

    @Test
    public void getValueObjectRu() {
        ResourceProvider provider = new ResourceProvider("resources.Labels", Locale.getDefault());
        String testLabel = provider.getValue("TestLabel", "0");
        assertEquals("Тестовое сообщение", testLabel);
    }

}