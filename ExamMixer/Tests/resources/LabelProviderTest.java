package resources;

import org.junit.Before;
import org.junit.Test;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

public class LabelProviderTest {

    @Before
    public void setUp() {
        ResourceBundle.clearCache();
    }

    @Test
    public void getValue() {
        String testLabel = LabelProvider.get("TestLabel", "0");
        assertEquals("Тестовое сообщение", testLabel);
    }

}