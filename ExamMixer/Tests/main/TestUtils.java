package main;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestUtils {

    public interface Action {
        void act() throws Exception;
    }


    private static final ExecutorService executorService = Executors.newWorkStealingPool();

    public static void executeInAnotherThread(Action action) throws InterruptedException {
        executorService.execute(() -> {
            try {
                action.act();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Thread.sleep(100);
    }

    public static File writeTempFile(Object user, String suffix, String content) throws IOException {
        Path path = Files.createTempFile(user.getClass().getSimpleName(), suffix);
        File file = new File(path.toString());
        file.deleteOnExit();
        Files.write(path, List.of(content), StandardCharsets.UTF_8);
        return file;
    }

    public static File getTempFile(Object user, String suffix) throws IOException {
        Path path = Files.createTempFile(user.getClass().getSimpleName(), suffix);
        File file = new File(path.toString());
        file.deleteOnExit();
        return file;
    }

    public static Object getField(Object object, String fieldName) throws Exception {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.get(object);
    }

    public static void assertFiles(File expected, File actual) throws IOException {
        try (Reader expectedReader = new FileReader(expected);
             BufferedReader expectedBufferedReader = new BufferedReader(expectedReader);
             Reader actualReader = new FileReader(actual);
             BufferedReader actualBufferedReader = new BufferedReader(actualReader)) {
            String expectedLine = expectedBufferedReader.readLine();
            String actualLine = actualBufferedReader.readLine();
            while (Objects.nonNull(expectedLine) && Objects.nonNull(actualLine)) {
                expectedLine = expectedLine.trim();
                actualLine = actualLine.trim();
                assertEquals(expectedLine, actualLine);
                expectedLine = expectedBufferedReader.readLine();
                actualLine = actualBufferedReader.readLine();
            }
            assertNull(expectedLine);
            assertNull(actualLine);
        }
    }

}
