package main;

import files.TextLoader;
import org.junit.Before;
import org.junit.Test;
import sources.Questions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class QuestionsControllerTest {

    private final QuestionsController questionsController = new QuestionsController(50, TimeUnit.MILLISECONDS);
    private Questions questions;

    @Before
    public void setUp() throws Exception {
        questions = (Questions) TestUtils.getField(questionsController, "questions");
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void loadText() throws InterruptedException, TextLoader.TextFormatException, TimeLock.TimeLockException,
            IOException, Questions.QuestionNotFoundException {

        String templateSource =
                "No identifier before. Not used.\n" +

                        "-IgNobel-Default\n" +
                        "Ig Nobel Prize winner 2017\n" +
                        "\n" +
                        "-IgNobel-Physics\n" +
                        "Physics\n" +
                        "Marc-Antoine Fardin\n" +
                        "\n" +
                        "-IgNobel-Anatomy\n" +
                        "Anatomy\n" +
                        "James Heathcote\n" +
                        "-IgNobel-Economics\n" +
                        "Economics\n" +
                        "Matthew Rockloff\n" +
                        "Nancy Greer\n" +

                        "-Ronaldo-Brazilian\n" +
                        "Ronaldo Luís Nazário de Lima\n" +
                        "-Ronaldo-Portuguese\n" +
                        "Cristiano Ronaldo dos Santos Aveiro\n" +

                        "-Word-English\n" +
                        "identifier\n" +
                        "-Word-Spanish\n" +
                        "identificador\n" +
                        "-Word-Italian\n" +
                        "identificatore";

        File file = TestUtils.writeTempFile(this, "idNotFoundException", templateSource);
        questionsController.loadText(file);

        assertEquals(Set.of("IgNobel", "Ronaldo", "Word"), questions.getQuestionsIds());

        assertEquals(Set.of("Physics", "Anatomy", "Economics"), questions.getOptionsIds("IgNobel"));
        assertEquals(Set.of("Brazilian", "Portuguese"), questions.getOptionsIds("Ronaldo"));
        assertEquals(Set.of("English", "Spanish", "Italian"), questions.getOptionsIds("Word"));

        assertEquals(List.of("Ig Nobel Prize winner 2017", "Physics", "Marc-Antoine Fardin"),
                questions.getText("IgNobel", "Physics"));
        assertEquals(List.of("Ig Nobel Prize winner 2017", "Anatomy", "James Heathcote"),
                questions.getText("IgNobel", "Anatomy"));
        assertEquals(List.of("Ig Nobel Prize winner 2017", "Economics", "Matthew Rockloff", "Nancy Greer"),
                questions.getText("IgNobel", "Economics"));

        assertEquals(List.of("identifier"),
                questions.getText("Word", "English"));
        assertEquals(List.of("identificador"),
                questions.getText("Word", "Spanish"));
        assertEquals(List.of("identificatore"),
                questions.getText("Word", "Italian"));

        assertEquals(List.of("Ronaldo Luís Nazário de Lima"),
                questions.getText("Ronaldo", "Brazilian"));
        assertEquals(List.of("Cristiano Ronaldo dos Santos Aveiro"),
                questions.getText("Ronaldo", "Portuguese"));

    }

    @Test
    public void loadImages() throws TimeLock.TimeLockException, InterruptedException {
        List<File> files = List.of(
                new File("Tests\\main\\TestFiles\\TestText.txt"),
                new File("Tests\\main\\Alpha-D\\"),
                new File("Tests\\main\\Alpha-D\\a"),
                new File("images\\One-4.bmp"),
                new File("C:\\images\\Two-B.png")
        );
        List<Boolean> results = questionsController.loadImages(files);
        assertEquals(List.of(false, true, false, true, true), results);
    }
}