package main;

import files.TemplateLoader;
import org.junit.Before;
import org.junit.Test;
import sources.DocumentBuilder;
import sources.Questions;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TemplatesControllerTest {

    private final TemplatesController templatesController = new TemplatesController(50, TimeUnit.MILLISECONDS);
    private TimeLock lock;

    @Before
    public void setUp() throws Exception {
        lock = (TimeLock) TestUtils.getField(templatesController, "lock");
    }

    // region Wrong Template Exception

    @Test(expected = TemplatesController.UnknownTemplateException.class)
    public void absentTemplateException() throws Exception {
        String templateSource = "...\n" +
                "EMGenerate-Article\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "...\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "...\n" +
                "EMTemplate-Begin-Another\n" +
                "...\n" +
                "EMTemplate-End-Another\n" +
                "...\n";
        File file = TestUtils.writeTempFile(this, "absentTemplateException", templateSource);
        templatesController.load(file);
    }

    @Test(expected = TemplateLoader.IncompleteTemplateException.class)
    public void incompleteTemplateException() throws Exception {
        String templateSource = "...\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "...\n" +
                "\n" +
                "...\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        File file = TestUtils.writeTempFile(this, "incompleteTemplateException", templateSource);
        templatesController.load(file);
    }

    @Test(expected = TemplateLoader.UnexpectedTemplateEndException.class)
    public void anotherTemplateEndException() throws Exception {
        String templateSource = "...\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "...\n" +
                "EMTemplate-End-Image\n" +
                "...\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "...\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        File file = TestUtils.writeTempFile(this, "anotherTemplateEndException", templateSource);
        templatesController.load(file);
    }

    @Test(expected = TemplateLoader.UnexpectedTemplateEndException.class)
    public void unknownTemplateEndException() throws Exception {
        String templateSource = "...\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "...\n" +
                "EMTemplate-End-Paper\n" +
                "\n" +
                "...\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        File file = TestUtils.writeTempFile(this, "unknownTemplateEndException", templateSource);
        templatesController.load(file);
    }

    // endregion Wrong Template Exception

    // region Correct Template

    @Test
    public void generatePairWithoutImages() throws Exception {
        String templateSource = "...\n" +
                "Generated text\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "Number EMIndex!\n" +
                "...\n" +
                "EMRandom-Id1\n" +
                "Separator\n" +
                "EMRandom-Id2\n" +
                "EMTemplate-End-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-TextLine\n" +
                "EMTextLine \n" +
                "EMTemplate-End-TextLine\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "Image here EMImagePath.\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        String templateResult = "...\n" +
                "Generated text\n" +
                "Number \n" +
                "1!\n" +
                "...\n" +
                "Id1[1]\n" +
                "Separator\n" +
                "Id2[1]\n" +
                "Number \n" +
                "2!\n" +
                "...\n" +
                "Id1[2]\n" +
                "Separator\n" +
                "Id2[2]\n" +
                "...\n" +
                "...\n" +
                "...";
        File file = TestUtils.writeTempFile(this, "generatePairWithoutImages", templateSource);
        File expected = TestUtils.writeTempFile(this, "generatePairWithoutImagesExpected", templateResult);
        templatesController.load(file);
        File output = TestUtils.getTempFile(this, "output");
        DocumentBuilder.ContentProvider contentProvider = (index, contentId, writer) ->
                templatesController.generateText(List.of(String.format("%s[%d]", contentId, index)), writer);
        templatesController.generate(output, contentProvider, 2);
        TestUtils.assertFiles(expected, output);
    }

    @Test
    public void generatePairWithImages() throws Exception {
        String templateSource = "...\n" +
                "Generated text\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "Number EMIndex!\n" +
                "...\n" +
                "EMRandom-Id1\n" +
                "Separator\n" +
                "EMRandom-Id2\n" +
                "EMTemplate-End-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-TextLine\n" +
                "EMTextLine \n" +
                "EMTemplate-End-TextLine\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "Image here EMImagePath.\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        String templateResult = "...\n" +
                "Generated text\n" +
                "Number \n" +
                "1!\n" +
                "...\n" +
                "Id1[1]\n" +
                "Separator\n" +
                "Id2[1]\n" +
                "...\n" +
                "Image here \n" +
                "Image.jpeg.\n" +
                "...\n" +
                "Image here \n" +
                "C:\\A\\Image.png.\n" +
                "Number \n" +
                "2!\n" +
                "...\n" +
                "Id1[2]\n" +
                "Separator\n" +
                "Id2[2]\n" +
                "...\n" +
                "Image here \n" +
                "Image.jpeg.\n" +
                "...\n" +
                "Image here \n" +
                "C:\\A\\Image.png.\n" +
                "...\n" +
                "...\n" +
                "...";
        File file = TestUtils.writeTempFile(this, "generatePairWithImages", templateSource);
        File expected = TestUtils.writeTempFile(this, "generatePairWithImagesExpected", templateResult);
        templatesController.load(file);
        File output = TestUtils.getTempFile(this, "output");
        DocumentBuilder.ContentProvider contentProvider = (index, contentId, writer) -> {
            templatesController.generateText(List.of(String.format("%s[%d]", contentId, index)), writer);
            if (contentId.equals("Id2")) {
                templatesController.generateImages(List.of("Image.jpeg", "C:\\A\\Image.png"), writer);
            }
        };
        templatesController.generate(output, contentProvider, 2);
        TestUtils.assertFiles(expected, output);
    }

    // endregion Correct Template

    // region Lock Exception

    @Test(expected = TimeLock.TimeLockException.class)
    public void timeLockWriteAfterReadException() throws Exception {
        lock.readLock();
        File file = TestUtils.writeTempFile(this, "timeLockWriteAfterReadException", "...");
        templatesController.load(file);
    }

    @Test(expected = TimeLock.TimeLockException.class)
    public void timeLockWriteAfterWriteException() throws Exception {
        TestUtils.executeInAnotherThread(() -> lock.writeLock());
        File file = TestUtils.writeTempFile(this, "timeLockWriteAfterWriteException", "...");
        templatesController.load(file);
    }

    @Test(expected = TimeLock.TimeLockException.class)
    public void timeLockReadAfterWriteException() throws Exception {
        File file = TestUtils.writeTempFile(this, "timeLockReadAfterWriteException", "...");
        templatesController.load(file);
        TestUtils.executeInAnotherThread(() -> lock.writeLock());
        File output = TestUtils.getTempFile(this, "output");
        DocumentBuilder.ContentProvider contentProvider = (index, contentId, writer) -> {
        };
        templatesController.generate(output, contentProvider, 2);
    }

    // endregion Lock Exception

    // region Content Provider Exception

    @Test(expected = Questions.IdNotFoundException.class)
    public void idNotFoundException() throws Exception {
        String templateSource = "...\n" +
                "Generated text\n" +
                "EMGenerate-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-Paper\n" +
                "Number EMIndex!\n" +
                "...\n" +
                "EMRandom-Id1\n" +
                "Separator\n" +
                "EMRandom-Id2\n" +
                "EMTemplate-End-Paper\n" +
                "...\n" +
                "EMTemplate-Begin-TextLine\n" +
                "EMTextLine \n" +
                "EMTemplate-End-TextLine\n" +
                "EMTemplate-Begin-Image\n" +
                "...\n" +
                "Image here EMImagePath.\n" +
                "EMTemplate-End-Image\n" +
                "...\n";
        File file = TestUtils.writeTempFile(this, "idNotFoundException", templateSource);
        templatesController.load(file);
        File output = TestUtils.getTempFile(this, "output");
        DocumentBuilder.ContentProvider contentProvider = (index, contentId, writer) -> {
            templatesController.generateText(List.of(String.format("%s[%d]", contentId, index)), writer);
            if (contentId.equals("Id2") && index == 2) {
                throw new Questions.IdNotFoundException(contentId);
            }
        };
        templatesController.generate(output, contentProvider, 2);
    }

    // endregion Content Provider Exception

}