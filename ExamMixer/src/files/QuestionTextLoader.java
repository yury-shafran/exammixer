package files;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class QuestionTextLoader implements TextLoader.LineProcessor {

    public interface Storage {

        void addOptionText(String questionId, String optionId, List<String> text);

    }

    private final Storage storage;
    private final IdRecognizer idRecognizer;

    public QuestionTextLoader(Storage storage, IdRecognizer idRecognizer) {
        this.storage = storage;
        this.idRecognizer = idRecognizer;
    }

    // region LineProcessor

    private IdRecognizer.Ids stateIds = null;
    private final LinkedList<String> stateText = new LinkedList<>();

    @Override
    public void prepare() {
        stateIds = null;
        stateText.clear();
    }

    @Override
    public void process(String line) {
        IdRecognizer.Ids newIds = idRecognizer.recognize(line);
        if (Objects.nonNull(newIds)) {
            store();
            stateIds = newIds;
        } else {
            stateText.add(line);
        }
    }

    @Override
    public void finish() {
        store();
    }

    private void store() {
        if (Objects.nonNull(stateIds)) {
            if (stateText.size() > 0 && stateText.getLast().isEmpty()) {
                stateText.pollLast();
            }
            storage.addOptionText(stateIds.getQuestionId(), stateIds.getOptionId(), stateText);
        }
        prepare();
    }

    // endregion LineProcessor

}
