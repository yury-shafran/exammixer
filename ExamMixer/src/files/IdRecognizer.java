package files;

public interface IdRecognizer {

    Ids recognize(String text);

    class Ids {

        private final String questionId, optionId;

        public Ids(String questionId, String optionId) {
            this.questionId = questionId;
            this.optionId = optionId;
        }

        public String getQuestionId() {
            return questionId;
        }

        public String getOptionId() {
            return optionId;
        }

    }

}
