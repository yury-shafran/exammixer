package files;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class QuestionImageLoader {

    public interface Storage {

        void addOptionImage(String questionId, String optionId, String imagePath);

    }

    private final Storage storage;
    private final IdRecognizer idRecognizer;

    public QuestionImageLoader(Storage storage, IdRecognizer idRecognizer) {
        this.storage = storage;
        this.idRecognizer = idRecognizer;
    }

    public List<Boolean> load(List<File> imageFiles) {
        return imageFiles
                .stream()
                .map(this::load)
                .collect(Collectors.toList());
    }

    private boolean load(File file) {
        boolean result;
        String absolutePath = file.getAbsolutePath();
        Path path = Paths.get(absolutePath);
        IdRecognizer.Ids ids = idRecognizer.recognize(path.getFileName().toString());
        if (Objects.nonNull(ids)) {
            storage.addOptionImage(ids.getQuestionId(), ids.getOptionId(), absolutePath);
            result = true;
        } else {
            result = false;
        }
        return result;
    }

}
