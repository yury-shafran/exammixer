package files;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;
import java.util.regex.Matcher;

public class TemplateLoader implements TextLoader.LineProcessor {

    public interface Storage {

        void add(String templateId, String line, boolean literal);

    }

    public interface TemplateRecognizer {

        Matcher match(String line);

        String beginTemplateId(Matcher matcher);

        String endTemplateId(Matcher matcher);

        String keyword(Matcher matcher);

    }

    public static class IncompleteTemplateException extends TextLoader.TextFormatException {

        private final String templateId;
        private final int templateFirstLineIndex;

        public IncompleteTemplateException(String templateId, int templateFirstLineIndex, int lineIndex) {
            super(lineIndex);
            this.templateId = templateId;
            this.templateFirstLineIndex = templateFirstLineIndex;
        }

        public String getTemplateId() {
            return templateId;
        }

        public int getTemplateFirstLineIndex() {
            return templateFirstLineIndex;
        }

    }

    public static class UnexpectedTemplateEndException extends TextLoader.TextFormatException {

        public static final String NothingExpected = "NothingExpected";

        private final String expectedTemplateId, actualTemplateId;
        private final int expectedTemplateFirstLineIndex;

        public UnexpectedTemplateEndException(String expectedTemplateId, String actualTemplateId,
                                              int expectedTemplateFirstLineIndex, int lineIndex) {
            super(lineIndex);
            this.expectedTemplateId = expectedTemplateId;
            this.actualTemplateId = actualTemplateId;
            this.expectedTemplateFirstLineIndex = expectedTemplateFirstLineIndex;
        }

        public UnexpectedTemplateEndException(String actualTemplateId, int lineIndex) {
            this(NothingExpected, actualTemplateId, -1, lineIndex);
        }

        public String getExpectedTemplateId() {
            return expectedTemplateId;
        }

        public String getActualTemplateId() {
            return actualTemplateId;
        }

        public int getExpectedTemplateFirstLineIndex() {
            return expectedTemplateFirstLineIndex;
        }

    }

    private final Storage storage;
    private final TemplateRecognizer recognizer;
    private final String documentTemplateId;

    public TemplateLoader(Storage storage, TemplateRecognizer recognizer, String documentTemplateId) {
        this.storage = storage;
        this.recognizer = recognizer;
        this.documentTemplateId = documentTemplateId;
    }

    // region LineProcessor

    private final Deque<String> templateIdsStack = new LinkedList<>();
    private final Deque<Integer> templateFirstLineIndices = new LinkedList<>();
    private int currentLineIndex;

    @Override
    public void prepare() {
        templateIdsStack.clear();
        templateIdsStack.push(documentTemplateId);
        currentLineIndex = 1;
        templateFirstLineIndices.clear();
        templateFirstLineIndices.push(currentLineIndex);
    }

    @Override
    public void process(String line) throws TextLoader.TextFormatException {
        Matcher matcher = recognizer.match(line);
        int charIndex = 0;
        while (matcher.find()) {
            if (matcher.start() > charIndex) {
                String literalBeforeMatch = line.substring(charIndex, matcher.start());
                storage.add(templateIdsStack.peek(), literalBeforeMatch, true);
            }
            processKeyword(matcher);
            charIndex = matcher.end();
        }
        if (line.length() > charIndex) {
            String literalAfterMatch = line.substring(charIndex);
            storage.add(templateIdsStack.peek(), literalAfterMatch, true);
        }
        currentLineIndex++;
    }

    private void processKeyword(Matcher matcher) throws TextLoader.TextFormatException {
        String beginTemplateId = recognizer.beginTemplateId(matcher);
        String endTemplateId = recognizer.endTemplateId(matcher);
        if (Objects.nonNull(beginTemplateId)) {
            templateIdsStack.push(beginTemplateId);
            templateFirstLineIndices.push(currentLineIndex);
        } else if (Objects.nonNull(endTemplateId)) {
            if (templateIdsStack.isEmpty()) {
                throw new UnexpectedTemplateEndException(endTemplateId, currentLineIndex);
            } else if (!endTemplateId.equals(templateIdsStack.peek())) {
                //noinspection ConstantConditions
                throw new UnexpectedTemplateEndException(templateIdsStack.peek(), endTemplateId,
                        templateFirstLineIndices.peek(), currentLineIndex);
            } else {
                templateIdsStack.pop();
                templateFirstLineIndices.pop();
            }
        } else {
            storage.add(templateIdsStack.peek(), recognizer.keyword(matcher), false);
        }
    }

    @Override
    public void finish() throws TextLoader.TextFormatException {
        if (templateIdsStack.size() > 1) {
            //noinspection ConstantConditions
            throw new IncompleteTemplateException(
                    templateIdsStack.peek(), templateFirstLineIndices.peek(), currentLineIndex);
        }
    }

    // endregion LineProcessor

}
