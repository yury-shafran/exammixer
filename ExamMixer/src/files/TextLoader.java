package files;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;

public class TextLoader {

    private final LineProcessor lineProcessor;

    public TextLoader(LineProcessor lineProcessor) {
        this.lineProcessor = lineProcessor;
    }

    public void load(BufferedReader bufferedReader) throws IOException, TextFormatException {
        lineProcessor.prepare();
        String line = bufferedReader.readLine();
        while (Objects.nonNull(line)) {
            lineProcessor.process(line);
            line = bufferedReader.readLine();
        }
        lineProcessor.finish();
    }

    interface LineProcessor {

        void prepare();

        void process(String line) throws TextFormatException;

        void finish() throws TextFormatException;

    }

    public static class TextFormatException extends Exception {

        private final int lineIndex;

        public TextFormatException(int lineIndex) {
            this.lineIndex = lineIndex;
        }

        public int getLineIndex() {
            return lineIndex;
        }

    }

}
