package main;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;

public class ProgressController {

    private static final double defaultSize = 24;
    private static final Paint defaultPaint = Paint.valueOf(Color.DARKGREEN.toString());
    private static final double marginPart = 0.2, leftPart = 0.45, startYPart = 0.5;
    private final ProgressIndicator progressIndicator = new ProgressIndicator();
    private final Pane pane = new Pane();

    public ProgressController(GridPane gridPane, int columnIndex, int rowIndex,
                              double width, double height, HPos hPos, VPos vPos, Paint paint) {
        gridPane.getChildren().addAll(progressIndicator, pane);
        place(progressIndicator, columnIndex, rowIndex, width, height, hPos, vPos);
        place(pane, columnIndex, rowIndex, width, height, hPos, vPos);
        Line left = new Line(
                width * marginPart, height * startYPart, width * leftPart, height * (1 - marginPart));
        Line right = new Line(
                width * leftPart, height * (1 - marginPart), width * (1 - marginPart), height * marginPart);
        left.setStroke(paint);
        right.setStroke(paint);
        left.setStrokeWidth(defaultSize * 0.2);
        right.setStrokeWidth(defaultSize * 0.2);
        pane.getChildren().addAll(left, right);
    }

    public ProgressController(GridPane gridPane, int columnIndex, int rowIndex, double width, double height) {
        this(gridPane, columnIndex, rowIndex, width, height, HPos.CENTER, VPos.CENTER, defaultPaint);
    }

    public ProgressController(GridPane gridPane, int columnIndex, int rowIndex) {
        this(gridPane, columnIndex, rowIndex, defaultSize, defaultSize, HPos.CENTER, VPos.CENTER, defaultPaint);
    }

    public void showProgress() {
        progressIndicator.setVisible(true);
        pane.setVisible(false);
    }

    public void showTick() {
        progressIndicator.setVisible(false);
        pane.setVisible(true);
    }

    public void hide() {
        progressIndicator.setVisible(false);
        pane.setVisible(false);
    }

    private void place(Region region, int columnIndex, int rowIndex, double width, double height, HPos hPos, VPos vPos) {
        region.setPrefWidth(width);
        region.setPrefHeight(height);
        GridPane.setColumnIndex(region, columnIndex);
        GridPane.setRowIndex(region, rowIndex);
        GridPane.setHalignment(region, hPos);
        GridPane.setValignment(region, vPos);
        region.setVisible(false);
    }

}
