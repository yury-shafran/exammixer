package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import resources.LabelProvider;

import java.util.ResourceBundle;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(
                getClass().getResource("main.fxml"),
                ResourceBundle.getBundle("resources.Labels")
        );
        primaryStage.setTitle(LabelProvider.get("WindowTitle", "ExamMixer"));
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

}
