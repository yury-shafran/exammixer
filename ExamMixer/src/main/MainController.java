package main;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import resources.LabelProvider;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainController implements Initializable, AsyncController.User<List<String>> {

    // region UI

    private final FileChooser textFilesChooser = new FileChooser();
    private final FileChooser imageFilesChooser = new FileChooser();
    private final FileChooser templateFileChooser = new FileChooser();
    private final FileChooser resultFileChooser = new FileChooser();
    private final DirectoryChooser textDirectoryChooser = new DirectoryChooser();
    private final DirectoryChooser imageDirectoryChooser = new DirectoryChooser();

    private final SimpleObjectProperty<File> currentDirectory = new SimpleObjectProperty<>(
            this, "currentDirectory", new File(Paths.get(".").toString()));

    @FXML
    private TreeTableView questionsTreeTable;
    @FXML
    private ListView logList;
    @FXML
    private Spinner generationQuantitySpinner;
    @FXML
    private GridPane MainGrid;
    private ProgressController textProgressView, imageProgressView, templateProgressView, generationProgressView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setChoosers();
        setProgressControls();
        setMapping();
        setQuestionsView();
    }

    private void setQuestionsView() {
        questionsTreeTable.setShowRoot(false);
        TreeTableColumn<Map.Entry<List<String>, List<String>>, String> optionTextColumn =
                new TreeTableColumn<>(LabelProvider.get("TextColumn", "Text"));
        optionTextColumn.setPrefWidth(350);
        optionTextColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Map.Entry<List<String>, List<String>>, String> param) ->
                        new ReadOnlyStringWrapper(
                                String.join("\n", param.getValue().getValue().getKey())));
        TreeTableColumn<Map.Entry<List<String>, List<String>>, String> imagesTextColumn =
                new TreeTableColumn<>(LabelProvider.get("ImagesColumn", "Images"));
        imagesTextColumn.setPrefWidth(350);
        imagesTextColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Map.Entry<List<String>, List<String>>, String> param) ->
                        new ReadOnlyStringWrapper(
                                String.join("\n", param.getValue().getValue().getValue())));
        questionsTreeTable.getColumns().setAll(optionTextColumn, imagesTextColumn);
    }

    private void updateDirectory(File file, boolean parent) {
        if (parent) {
            String parentPath = file.toPath().getParent().toString();
            currentDirectory.set(new File(parentPath));
        } else {
            currentDirectory.set(file);
        }
    }

    private void setChoosers() {
        textFilesChooser.setTitle(LabelProvider.get("ChooseTextFiles",
                "Choose text files with questions..."));
        imageFilesChooser.setTitle(LabelProvider.get("ChooseImageFiles",
                "Choose image files for questions..."));
        templateFileChooser.setTitle(LabelProvider.get("ChooseTemplateFile",
                "Choose text file with template..."));
        resultFileChooser.setTitle(LabelProvider.get("ChooseResultFile",
                "Choose file to save result to..."));
        textDirectoryChooser.setTitle(LabelProvider.get("ChooseTextDirectory",
                "Choose directory of text files with questions..."));
        imageDirectoryChooser.setTitle(LabelProvider.get("ChooseImageDirectory",
                "Choose directory of image files for questions..."));

        FileChooser.ExtensionFilter anyFileFilter = new FileChooser.ExtensionFilter(
                LabelProvider.get("AllFilesFilter", "All files"), "*.*");
        textFilesChooser.getExtensionFilters()
                .addAll(
                        new FileChooser.ExtensionFilter(
                                LabelProvider.get("TextFilesFilter", "Text files"),
                                "*.txt"
                        ),
                        anyFileFilter
                );
        imageFilesChooser.getExtensionFilters()
                .addAll(
                        new FileChooser.ExtensionFilter(
                                LabelProvider.get("ImageFilesFilter", "Images"),
                                "*.png", "*.bmp", "*.gif", "*.jpeg", "*.jpg"
                        ),
                        anyFileFilter
                );
        templateFileChooser.getExtensionFilters().add(anyFileFilter);
        resultFileChooser.getExtensionFilters().add(anyFileFilter);

        textFilesChooser.initialDirectoryProperty().bind(currentDirectory);
        textDirectoryChooser.initialDirectoryProperty().bind(currentDirectory);
        imageFilesChooser.initialDirectoryProperty().bind(currentDirectory);
        imageDirectoryChooser.initialDirectoryProperty().bind(currentDirectory);
        templateFileChooser.initialDirectoryProperty().bind(currentDirectory);
        resultFileChooser.initialDirectoryProperty().bind(currentDirectory);
    }

    private void setProgressControls() {
        textProgressView = new ProgressController(MainGrid, 3, 0);
        imageProgressView = new ProgressController(MainGrid, 3, 1);
        templateProgressView = new ProgressController(MainGrid, 3, 2);
        generationProgressView = new ProgressController(MainGrid, 3, 4);
    }

    private Window getWindow() {
        return MainGrid.getScene().getWindow();
    }

    private void chooseFiles(FileChooser fileChooser, Consumer<List<File>> processor) {
        List<File> files = fileChooser.showOpenMultipleDialog(getWindow());
        if (Objects.nonNull(files) && !files.isEmpty()) {
            updateDirectory(files.get(0), true);
            processor.accept(files);
        }
    }

    private void chooseDirectory(DirectoryChooser directoryChooser, Consumer<List<File>> processor) {
        File directory = directoryChooser.showDialog(getWindow());
        if (Objects.nonNull(directory)) {
            updateDirectory(directory, false);
            List<File> files = unfoldDirectory(directory).collect(Collectors.toList());
            processor.accept(files);
        }
    }

    private Stream<File> unfoldDirectory(File directory) {
        File[] files = directory.listFiles();
        Stream<File> fileStream;
        if (Objects.nonNull(files)) {
            fileStream = Stream.of(files).flatMap(this::mapFile);
        } else {
            fileStream = Stream.of();
        }
        return fileStream;
    }

    private Stream<File> mapFile(File file) {
        Stream<File> mapping;
        if (file.isDirectory()) {
            mapping = unfoldDirectory(file);
        } else {
            mapping = Stream.of(file);
        }
        return mapping;
    }

    @FXML
    private void chooseTextFiles() {
        chooseFiles(textFilesChooser, this::loadText);
    }

    @FXML
    private void chooseImageFiles() {
        chooseFiles(imageFilesChooser, this::loadImages);
    }

    @FXML
    private void chooseTextDirectory() {
        chooseDirectory(textDirectoryChooser, this::loadText);
    }

    @FXML
    private void chooseImageDirectory() {
        chooseDirectory(imageDirectoryChooser, this::loadImages);
    }

    @FXML
    private void clearQuestions() {
        clearTextAndImages();
    }

    @FXML
    private void chooseTemplateFile() {
        File file = templateFileChooser.showOpenDialog(getWindow());
        if (Objects.nonNull(file)) {
            updateDirectory(file, true);
            loadTemplate(file);
        }
    }

    @FXML
    private void chooseResultFile() {
        File file = resultFileChooser.showSaveDialog(getWindow());
        if (Objects.nonNull(file)) {
            updateDirectory(file, true);
            generate(file);
        }
    }

    // endregion UI

    // region Processing

    private final int timeout = 5;
    private final TimeUnit timeUnit = TimeUnit.SECONDS;
    private final QuestionsController questionsController = new QuestionsController(timeout, timeUnit);
    private final TemplatesController templatesController = new TemplatesController(timeout, timeUnit);
    private final Generator generator = new Generator(questionsController, templatesController);
    private final AsyncController<List<String>> asyncController = new AsyncController<>(this);
    @SuppressWarnings("FieldCanBeLocal")
    private final String
            textLoadId = "textLoadId", imageLoadId = "imageLoadId",
            templateLoadId = "templateLoadId",
            generationId = "generationId",
            optionShowId = "optionShowId";
    private final Map<String, ProgressController> progressMapping = new HashMap<>();

    private Map<String, Map<String, Map.Entry<List<String>, List<String>>>> options;

    private void setMapping() {
        progressMapping.put(textLoadId, textProgressView);
        progressMapping.put(imageLoadId, imageProgressView);
        progressMapping.put(templateLoadId, templateProgressView);
        progressMapping.put(generationId, generationProgressView);
    }

    @Override
    public void onStateChanged(String id, AsyncController.TaskState state) {
        if (progressMapping.containsKey(id)) {
            if (state == AsyncController.TaskState.Running) {
                progressMapping.get(id).showProgress();
            } else {
                progressMapping.get(id).hide();
                if (id.equals(textLoadId) || id.equals(imageLoadId)) {
                    getOptions();
                }
            }
        }
    }

    @Override
    public void onFinished(String id, List<String> result) {
        //noinspection unchecked
        logList.getItems().addAll(result);
        if (id.equals(optionShowId)) {
            showOptions();
        }
    }

    private void clearTextAndImages() {
        asyncController.start(optionShowId, new Task<>() {
            @Override
            protected List<String> call() {
                List<String> result = List.of(questionsController.clearSafely());
                options = questionsController.getAllOptionsSafe();
                return result;
            }
        });
    }

    private void loadText(List<File> files) {
        asyncController.start(textLoadId, new Task<>() {
            @Override
            protected List<String> call() {
                return questionsController.loadTextSafely(files);
            }
        });
    }

    private void loadImages(List<File> files) {
        asyncController.start(imageLoadId, new Task<>() {
            @Override
            protected List<String> call() {
                return questionsController.loadImagesSafely(files);
            }
        });
    }

    private void getOptions() {
        asyncController.start(optionShowId, new Task<>() {
            @Override
            protected List<String> call() {
                options = questionsController.getAllOptionsSafe();
                return List.of();
            }
        });
    }

    private void showOptions() {
        TreeItem<Map.Entry<List<String>, List<String>>> root = new TreeItem<>(
                Map.entry(List.of(), List.of()));
        questionsTreeTable.setRoot(root);
        for (String questionId : options.keySet()) {
            TreeItem<Map.Entry<List<String>, List<String>>> question = new TreeItem<>(
                    Map.entry(List.of(questionId), List.of()));
            question.setExpanded(true);
            root.getChildren().add(question);
            Map<String, Map.Entry<List<String>, List<String>>> questionOptions = options.get(questionId);
            for (Map.Entry<List<String>, List<String>> option : questionOptions.values()) {
                TreeItem<Map.Entry<List<String>, List<String>>> item = new TreeItem<>(option);
                question.getChildren().add(item);
            }
        }
    }

    private void loadTemplate(File file) {
        asyncController.startSingle(templateLoadId, new Task<>() {
            @Override
            protected List<String> call() {
                return List.of(templatesController.loadSafe(file));
            }
        });
    }

    private void generate(File file) {
        int count;
        try {
            Object value = generationQuantitySpinner.getValue();
            count = Integer.parseInt(value.toString());
            if (count < 1) {
                throw new NullPointerException();
            }
            asyncController.startSingle(generationId, new Task<>() {
                @Override
                protected List<String> call() {
                    return List.of(generator.generateSafe(file, count));
                }
            });
        } catch (NullPointerException | NumberFormatException e) {
            String message = LabelProvider.get("IncorrectCount", "Incorrect count.");
            //noinspection unchecked
            logList.getItems().add(message);
        }
    }

    // endregion Processing

}
