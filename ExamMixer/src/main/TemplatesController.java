package main;

import files.TemplateLoader;
import files.TextLoader;
import resources.LabelProvider;
import sources.DocumentBuilder;
import sources.Templates;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TemplatesController {

    private final Templates templates = new Templates();
    private final TimeLock lock;

    public TemplatesController(int time, TimeUnit unit) {
        lock = new TimeLock(true, time, unit);
    }

    // region Ids

    public static class UnknownTemplateException extends Exception {

        private final Set<String> templatesIds;

        public UnknownTemplateException(Set<String> unknownTemplatesIds) {
            this.templatesIds = unknownTemplatesIds;
        }

        public Set<String> getTemplatesIds() {
            return templatesIds;
        }

    }

    private static final String documentTemplateId = "EMDocument";
    private static final String beginTemplateIdGroup = "beginTemplateId";
    private static final String endTemplateIdGroup = "endTemplateId";
    private static final String randomIdGroup = "randomId";
    private static final String generationTemplateId = "generationTemplateId";
    private static final String indexKeyword = "EMIndex", textLineKeyword = "EMTextLine", imagePathKeyword = "EMImagePath";
    private static final String textLineTemplateId = "TextLine", imagePathTemplateId = "Image";
    private static final String regex;
    private static final Pattern pattern, randomPattern, generationPattern;
    private static final Predicate<String> randomPredicate, generationPredicate;

    static {
        String beginTemplateRegex = "^EMTemplate-Begin-(?<" + beginTemplateIdGroup + ">[\\wёЁа-яА-Я]+).*$";
        String endTemplateRegex = "^EMTemplate-End-(?<" + endTemplateIdGroup + ">[\\wёЁа-яА-Я]+).*$";
        String randomRegex = "EMRandom-(?<" + randomIdGroup + ">[\\wёЁа-яА-Я]+)";
        String generateRegex = "EMGenerate-(?<" + generationTemplateId + ">[\\wёЁа-яА-Я]+)";
        randomPattern = Pattern.compile(randomRegex);
        generationPattern = Pattern.compile(generateRegex);
        randomPredicate = randomPattern.asPredicate();
        generationPredicate = generationPattern.asPredicate();
        Stream<String> regs = Stream.of(beginTemplateRegex, endTemplateRegex, randomRegex, generateRegex);
        regs = Stream.concat(regs, Stream.of(indexKeyword, textLineKeyword, imagePathKeyword));
        regex = regs.collect(Collectors.joining("|"));
        pattern = Pattern.compile(regex);
    }

    // endregion Ids

    // region Load

    private static class CheckedTemplateRecognizer implements TemplateLoader.TemplateRecognizer {

        private final Set<String> linkedTemplatesIds = new HashSet<>(), foundTemplatesIds = new HashSet<>();

        @Override
        public Matcher match(String line) {
            return pattern.matcher(line);
        }

        @Override
        public String beginTemplateId(Matcher matcher) {
            String templateId = matcher.group(beginTemplateIdGroup);
            foundTemplatesIds.add(templateId);
            return templateId;
        }

        @Override
        public String endTemplateId(Matcher matcher) {
            return matcher.group(endTemplateIdGroup);
        }

        @Override
        public String keyword(Matcher matcher) {
            String templateId = matcher.group(generationTemplateId);
            if (Objects.nonNull(templateId)) {
                linkedTemplatesIds.add(templateId);
            }
            return matcher.group();
        }

        public void checkIds() throws UnknownTemplateException {
            Set<String> tempSet = new HashSet<>(linkedTemplatesIds);
            tempSet.removeAll(foundTemplatesIds);
            if (!tempSet.isEmpty()) {
                throw new UnknownTemplateException(tempSet);
            }
        }

    }

    public void load(File file) throws
            IOException, InterruptedException, TimeLock.TimeLockException, TextLoader.TextFormatException, UnknownTemplateException {
        CheckedTemplateRecognizer templateRecognizer = new CheckedTemplateRecognizer();
        TemplateLoader templateLoader = new TemplateLoader(templates::add, templateRecognizer, documentTemplateId);
        TextLoader textLoader = new TextLoader(templateLoader);
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {
            lock.writeLock();
            templates.clear();
            textLoader.load(bufferedReader);
            templateRecognizer.checkIds();
        } finally {
            lock.writeUnlock();
        }
    }

    public String loadSafe(File file) {
        String result;
        try {
            load(file);
            result = LabelProvider.getSucceeded();
        } catch (TemplateLoader.IncompleteTemplateException e) {
            result = LabelProvider.get("IncompleteTemplateException",
                    "Incomplete template \"%s\" [lines %d:%d].");
            result = String.format(result, e.getTemplateId(), e.getTemplateFirstLineIndex(), e.getLineIndex());
        } catch (TemplateLoader.UnexpectedTemplateEndException e) {
            if (e.getExpectedTemplateFirstLineIndex() == -1) {
                result = LabelProvider.get("UnknownTemplateEndException",
                        "Unknown template \"%s\" ends at line %d.");
                result = String.format(result, e.getActualTemplateId(), e.getLineIndex());
            } else {
                result = LabelProvider.get("AnotherTemplateEndException",
                        "Template \"%s\" ends at line %d within template \"%s\" opened at line %d.");
                result = String.format(result, e.getExpectedTemplateId(), e.getExpectedTemplateFirstLineIndex(),
                        e.getActualTemplateId(), e.getLineIndex());
            }
        } catch (InterruptedException e) {
            result = LabelProvider.getInterruptedException();
        } catch (TimeLock.TimeLockException e) {
            result = LabelProvider.getTimeLockException();
        } catch (IOException | TextLoader.TextFormatException e) {
            result = e.getMessage();
        } catch (UnknownTemplateException e) {
            result = LabelProvider.get("UnknownTemplateException", "Unknown templates: %s.");
            result = String.format(result, e.getTemplatesIds().toString());
        }
        return LabelProvider.getLoadTemplateResult(file.getAbsolutePath(), result);
    }

    // endregion Load

    // region Generation

    private class PrimaryContentProvider implements DocumentBuilder.ContentProvider {

        private final DocumentBuilder.ContentProvider contentProvider;
        private final int count;

        private PrimaryContentProvider(DocumentBuilder.ContentProvider secondaryContentProvider, int countToGenerate) {
            contentProvider = secondaryContentProvider;
            count = countToGenerate;
        }

        @Override
        public void addContent(int index, String contentId, PrintWriter writer) throws Exception {
            if (generationPredicate.test(contentId) && index == 0) {
                generate(contentId, writer);
            } else if (randomPredicate.test(contentId)) {
                Matcher matcher = randomPattern.matcher(contentId);
                //noinspection ResultOfMethodCallIgnored
                matcher.find();
                String randomId = matcher.group(randomIdGroup);
                contentProvider.addContent(index, randomId, writer);
            } else if (contentId.equals(indexKeyword)) {
                writer.print(index);
            } else {
                contentProvider.addContent(index, contentId, writer);
            }
        }

        private void generate(String contentId, PrintWriter writer) throws Exception {
            Matcher matcher = generationPattern.matcher(contentId);
            //noinspection ResultOfMethodCallIgnored
            matcher.find();
            String templateId = matcher.group(generationTemplateId);
            DocumentBuilder documentBuilder = templates.get(templateId);
            if (Objects.isNull(documentBuilder)) {
                throw new UnknownTemplateException(Set.of(templateId));
            }
            for (int i = 1; i <= count; i++) {
                documentBuilder.build(i, this, writer);
            }
        }

    }

    public void generate(File file, DocumentBuilder.ContentProvider secondaryContentProvider, int count) throws Exception {
        DocumentBuilder documentBuilder = templates.get(documentTemplateId);
        if (Objects.isNull(documentBuilder)) {
            throw new UnknownTemplateException(Set.of());
        }
        PrimaryContentProvider primaryContentProvider = new PrimaryContentProvider(secondaryContentProvider, count);
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8);
             PrintWriter printWriter = new PrintWriter(bufferedWriter)) {
            lock.readLock();
            documentBuilder.build(0, primaryContentProvider, printWriter);
        } finally {
            lock.readUnlock();
        }
    }

    public boolean canAddText() {
        return Objects.nonNull(templates.get(textLineTemplateId));
    }

    public boolean canAddImages() {
        return Objects.nonNull(templates.get(imagePathTemplateId));
    }

    public void generateText(List<String> text, PrintWriter writer) throws Exception {
        try {
            lock.readLock();
            DocumentBuilder documentBuilder = templates.get(textLineTemplateId);
            DocumentBuilder.ContentProvider textProvider = new DocumentBuilder.ContentProvider() {
                private final Iterator<String> iterator = text.iterator();

                @Override
                public void addContent(int index, String contentId, PrintWriter writer) {
                    if (contentId.equals(textLineKeyword)) {
                        writer.print(iterator.next());
                    }
                }
            };
            if (Objects.nonNull(documentBuilder) && text.size() > 0) {
                for (int i = 0; i < text.size(); i++) {
                    documentBuilder.build(i, textProvider, writer);
                }
            }
        } finally {
            lock.readUnlock();
        }
    }

    public void generateImages(List<String> imagePaths, PrintWriter writer) throws Exception {
        try {
            lock.readLock();
            DocumentBuilder documentBuilder = templates.get(imagePathTemplateId);
            DocumentBuilder.ContentProvider imagePathProvider = new DocumentBuilder.ContentProvider() {
                private final Iterator<String> iterator = imagePaths.iterator();

                @Override
                public void addContent(int index, String contentId, PrintWriter writer) {
                    if (contentId.equals(imagePathKeyword)) {
                        writer.print(iterator.next());
                    }
                }
            };
            if (Objects.nonNull(documentBuilder) && imagePaths.size() > 0) {
                for (int i = 0; i < imagePaths.size(); i++) {
                    documentBuilder.build(i, imagePathProvider, writer);
                }
            }
        } finally {
            lock.readUnlock();
        }
    }

    // endregion Generation

}
