package main;

import resources.LabelProvider;
import sources.DocumentBuilder;
import sources.Questions;

import java.io.File;
import java.io.PrintWriter;

public class Generator {

    private final QuestionsController questionsController;
    private final TemplatesController templatesController;
    private int count;

    public Generator(QuestionsController questionsController, TemplatesController templatesController) {
        this.questionsController = questionsController;
        this.templatesController = templatesController;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @SuppressWarnings("unused")
    public void generate(File file) throws Exception {
        generate(file, count);
    }

    public void generate(File file, int count) throws Exception {
        templatesController.generate(file, getContentProvider(), count);
    }

    public String generateSafe(File file) {
        return generateSafe(file, count);
    }

    public String generateSafe(File file, int count) {
        String result;
        boolean removeFile = true;
        try {
            generate(file, count);
            result = LabelProvider.getSucceeded();
            removeFile = false;
        } catch (InterruptedException e) {
            result = LabelProvider.getInterruptedException();
        } catch (TimeLock.TimeLockException e) {
            result = LabelProvider.getTimeLockException();
        } catch (Questions.QuestionNotFoundException e) {
            result = LabelProvider.get("QuestionNotFound", "Options for \"%s\" were not found.");
            result = String.format(result, e.getId());
        } catch (TemplatesController.UnknownTemplateException e) {
            if (e.getTemplatesIds().isEmpty()) {
                result = LabelProvider.get("NoTemplateException", "No template.");
            } else {
                result = LabelProvider.get("UnknownTemplateException", "Unknown template: %s.");
                result = String.format(result, e.getTemplatesIds().toString());
            }
        } catch (Exception e) {
            result = e.getMessage();
        }
        if (removeFile) {
            //noinspection CatchMayIgnoreException
            try {
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            } catch (SecurityException e) {
            }
        }
        return LabelProvider.getGenerationResult(file.getAbsolutePath(), result);
    }

    private DocumentBuilder.ContentProvider getContentProvider() throws Exception {
        return new DocumentBuilder.ContentProvider() {

            private final QuestionsController.RandomOptionProvider randomOptionProvider =
                    questionsController.getRandomOptionProvider();

            @Override
            public void addContent(int index, String contentId, PrintWriter writer) throws Exception {
                QuestionsController.RandomOption option = randomOptionProvider.get(contentId);
                if (templatesController.canAddText()) {
                    templatesController.generateText(option.getText(), writer);
                }
                if (templatesController.canAddImages()) {
                    templatesController.generateImages(option.getImages(), writer);
                }
            }

        };
    }

}
