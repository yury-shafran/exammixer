package main;

import files.IdRecognizer;
import files.QuestionImageLoader;
import files.QuestionTextLoader;
import files.TextLoader;
import resources.LabelProvider;
import sources.Questions;
import sources.RandomOptionSuppliers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuestionsController {

    private final Questions questions = new Questions();
    private final TimeLock lock;

    public QuestionsController(int time, TimeUnit unit) {
        lock = new TimeLock(false, time, unit);
    }

    // region Ids

    private static final String questionIdGroup = "questionId", optionIdGroup = "optionId";
    private static final String idsRegex = "(?<" + questionIdGroup + ">[\\wёЁа-яА-Я]+?)-(?<" + optionIdGroup + ">[\\wёЁа-яА-Я]+).*";
    private static final String textIdsRegex = "-" + idsRegex;
    private static final String imageIdsRegex = ".*?" + idsRegex;
    private static final Pattern textIdsPattern = Pattern.compile(textIdsRegex);
    private static final Pattern imageIdsPattern = Pattern.compile(imageIdsRegex);

    private IdRecognizer.Ids recognizeTextIds(String text) {
        return recognizeIds(text, textIdsPattern);
    }

    private IdRecognizer.Ids recognizeImageIds(String text) {
        return recognizeIds(text, imageIdsPattern);
    }

    private IdRecognizer.Ids recognizeIds(String text, Pattern pattern) {
        IdRecognizer.Ids ids = null;
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            String questionId = matcher.group(questionIdGroup);
            String optionId = matcher.group(optionIdGroup);
            ids = new IdRecognizer.Ids(questionId, optionId);
        }
        return ids;
    }

    // endregion Ids

    // region Load

    public String clearSafely() {
        String result;
        try {
            lock.writeLock();
            questions.clear();
            result = LabelProvider.getSucceeded();
        } catch (InterruptedException e) {
            result = LabelProvider.getInterruptedException();
        } catch (TimeLock.TimeLockException e) {
            result = LabelProvider.getTimeLockException();
        } finally {
            lock.writeUnlock();
        }
        return LabelProvider.getClearResult(result);
    }

    public void loadText(File file) throws InterruptedException, IOException,
            TimeLock.TimeLockException, TextLoader.TextFormatException {
        QuestionTextLoader textLoader = new QuestionTextLoader(questions::addOptionText, this::recognizeTextIds);
        TextLoader loader = new TextLoader(textLoader);
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {
            lock.writeLock();
            loader.load(bufferedReader);
        } finally {
            lock.writeUnlock();
        }
    }

    public String loadTextSafely(File file) {
        String result;
        try {
            loadText(file);
            result = LabelProvider.get("TextFileProcessed", "Processed");
        } catch (InterruptedException e) {
            result = LabelProvider.getInterruptedException();
        } catch (IOException | TextLoader.TextFormatException e) {
            result = e.getMessage();
        } catch (TimeLock.TimeLockException e) {
            result = LabelProvider.getTimeLockException();
        }
        return LabelProvider.getLoadTextResult(file.getAbsolutePath(), result);
    }

    public List<String> loadTextSafely(List<File> files) {
        List<String> results = new ArrayList<>(files.size());
        for (File file : files) {
            results.add(loadTextSafely(file));
        }
        return results;
    }

    private void addOptionImage(String questionId, String optionId, String imagePath) {
        questions.addOptionImages(questionId, optionId, List.of(imagePath));
    }

    public List<Boolean> loadImages(List<File> files) throws TimeLock.TimeLockException, InterruptedException {
        QuestionImageLoader imageLoader = new QuestionImageLoader(this::addOptionImage, this::recognizeImageIds);
        List<Boolean> result;
        try {
            lock.writeLock();
            result = imageLoader.load(files);
        } finally {
            lock.writeUnlock();
        }
        return result;
    }

    public List<String> loadImagesSafely(List<File> files) {
        String format = LabelProvider.get("LoadImagesResult", "Image processing: %s");
        List<String> results = new ArrayList<>(files.size());
        try {
            List<Boolean> flags = loadImages(files);
            Iterator<Boolean> flagIterator = flags.iterator();
            for (File file : files) {
                String fileResult = LabelProvider.getLoadImageResult(
                        file.getAbsolutePath(),
                        flagIterator.next() ?
                                LabelProvider.get("LoadImageUsed", "Used.") :
                                LabelProvider.get("LoadImageSkipped", "Skipped."));
                results.add(fileResult);
            }
        } catch (InterruptedException e) {
            results = List.of(String.format(format, LabelProvider.getInterruptedException()));
        } catch (TimeLock.TimeLockException e) {
            results = List.of(String.format(format, LabelProvider.getTimeLockException()));
        }
        return results;
    }

    // endregion Load

    // region Generation

    public static class RandomOption {

        private final List<String> text;
        private final List<String> images;

        public RandomOption(List<String> text, List<String> images) {
            this.text = text;
            this.images = images;
        }

        public List<String> getText() {
            return text;
        }

        public List<String> getImages() {
            return images;
        }

    }

    public class RandomOptionProvider {

        private final RandomOptionSuppliers<String, String> randomOptionSuppliers;

        private RandomOptionProvider() throws InterruptedException, TimeLock.TimeLockException {
            try {
                lock.readLock();
                RandomOptionSuppliers<String, String>.Builder builder = RandomOptionSuppliers.builder();
                for (String questionId : questions.getQuestionsIds()) {
                    //noinspection CatchMayIgnoreException
                    try {
                        Set<String> optionsIds = questions.getOptionsIds(questionId);
                        builder.add(questionId, optionsIds);
                    } catch (Questions.QuestionNotFoundException e) {
                    }
                }
                randomOptionSuppliers = builder.build();
            } finally {
                lock.readUnlock();
            }
        }

        public RandomOption get(String questionId) throws TimeLock.TimeLockException, InterruptedException,
                Questions.QuestionNotFoundException {
            try {
                lock.readLock();
                if (!randomOptionSuppliers.contains(questionId)) {
                    throw new Questions.QuestionNotFoundException(questionId);
                }
                String optionId = randomOptionSuppliers.get(questionId);
                List<String> text = questions.getText(questionId, optionId);
                List<String> images = questions.getImages(questionId, optionId);
                return new RandomOption(text, images);
            } finally {
                lock.readUnlock();
            }
        }

    }

    public RandomOptionProvider getRandomOptionProvider() throws TimeLock.TimeLockException, InterruptedException {
        return new RandomOptionProvider();
    }

    // endregion Generation

    // region Getters

    public Map<String, Map<String, Map.Entry<List<String>, List<String>>>> getAllOptions() throws
            TimeLock.TimeLockException, InterruptedException, Questions.QuestionNotFoundException {
        try {
            lock.readLock();
            Map<String, Map<String, Map.Entry<List<String>, List<String>>>> map = new LinkedHashMap<>();
            Iterator<String> questionIterator = questions.getQuestionsIds().iterator();
            boolean go = !Thread.interrupted();
            while (questionIterator.hasNext() && go) {
                String questionId = questionIterator.next();
                Map<String, Map.Entry<List<String>, List<String>>> questionMap = new LinkedHashMap<>();
                map.put(questionId, questionMap);
                Iterator<String> optionIterator = questions.getOptionsIds(questionId).iterator();
                while (optionIterator.hasNext() && go) {
                    String optionId = optionIterator.next();
                    List<String> text = questions.getText(questionId, optionId);
                    List<String> images = questions.getImages(questionId, optionId);
                    Map.Entry<List<String>, List<String>> entry = Map.entry(text, images);
                    questionMap.put(optionId, entry);
                    go = !Thread.interrupted();
                }
            }
            return map;
        } finally {
            lock.readUnlock();
        }
    }

    public Map<String, Map<String, Map.Entry<List<String>, List<String>>>> getAllOptionsSafe() {
        Map<String, Map<String, Map.Entry<List<String>, List<String>>>> result = null;
        //noinspection CatchMayIgnoreException
        try {
            result = getAllOptions();
        } catch (InterruptedException | Questions.QuestionNotFoundException | TimeLock.TimeLockException e) {
        }
        return result;
    }

    // endregion Getters

}
