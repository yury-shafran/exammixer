package main;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncController<V> {

    enum TaskState {Running, Complete}

    interface User<V> {

        @SuppressWarnings("unused")
        void onStateChanged(String id, TaskState state);

        @SuppressWarnings("unused")
        void onFinished(String id, V result);

    }

    private abstract class Group {

        private final String id;

        private Group(String id) {
            this.id = id;
        }

        @SuppressWarnings("unused")
        abstract void start(Task<V> task);

        private void setAndStart(Task<V> task) {
            task.setOnScheduled(this::handleRunning);
            task.setOnRunning(this::handleRunning);
            EventHandler<WorkerStateEvent> completeHandler = event -> handleComplete(task, event);
            task.setOnSucceeded(completeHandler);
            task.setOnFailed(completeHandler);
            task.setOnCancelled(completeHandler);
            executorService.execute(task);
        }

        @SuppressWarnings("unused")
        private void handleRunning(WorkerStateEvent event) {
            user.onStateChanged(id, TaskState.Running);
        }

        @SuppressWarnings("unused")
        abstract void handleComplete(Task<V> task, WorkerStateEvent event);

    }

    private class SingleTaskGroup extends Group {

        private Task<V> currentTask, nextTask;

        private SingleTaskGroup(String id) {
            super(id);
        }

        @Override
        void start(Task<V> task) {
            if (Objects.nonNull(currentTask)) {
                currentTask.cancel(true);
                nextTask = task;
            } else {
                nextTask = null;
                currentTask = task;
                super.setAndStart(task);
            }
        }

        @Override
        void handleComplete(Task<V> task, WorkerStateEvent event) {
            user.onFinished(super.id, currentTask.getValue());
            currentTask = null;
            if (Objects.nonNull(nextTask)) {
                start(nextTask);
            } else {
                user.onStateChanged(super.id, TaskState.Complete);
            }
        }

    }

    private class MultiTaskGroup extends Group {

        private final Set<Task<V>> activeTasks = new HashSet<>();

        private MultiTaskGroup(String id) {
            super(id);
        }

        @Override
        void start(Task<V> task) {
            activeTasks.add(task);
            super.setAndStart(task);
        }

        @Override
        void handleComplete(Task<V> task, WorkerStateEvent event) {
            activeTasks.remove(task);
            user.onFinished(super.id, task.getValue());
            if (activeTasks.isEmpty()) {
                user.onStateChanged(super.id, TaskState.Complete);
            }
        }

    }

    private final User<V> user;
    private final ExecutorService executorService = Executors.newWorkStealingPool();
    private final Map<String, SingleTaskGroup> singleGroups = new HashMap<>();
    private final Map<String, MultiTaskGroup> multiGroups = new HashMap<>();

    public AsyncController(User<V> user) {
        this.user = user;
    }

    public void startSingle(String id, Task<V> task) {
        singleGroups.putIfAbsent(id, new SingleTaskGroup(id));
        singleGroups.get(id).start(task);
    }

    public void start(String id, Task<V> task) {
        multiGroups.putIfAbsent(id, new MultiTaskGroup(id));
        multiGroups.get(id).start(task);
    }

}
