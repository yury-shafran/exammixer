package main;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TimeLock {

    public static class TimeLockException extends Exception {

    }

    private final ReadWriteLock lock;
    private final long time;
    private final TimeUnit unit;

    public TimeLock(boolean fair, long time, TimeUnit unit) {
        lock = new ReentrantReadWriteLock(fair);
        this.time = time;
        this.unit = unit;
    }

    public void writeLock() throws InterruptedException, TimeLockException {
        timeLock(lock.writeLock());
    }

    public void writeUnlock() {
        safeUnlock(lock.writeLock());
    }

    public void readLock() throws InterruptedException, TimeLockException {
        timeLock(lock.readLock());
    }

    public void readUnlock() {
        safeUnlock(lock.readLock());
    }

    private void timeLock(Lock lock) throws InterruptedException, TimeLockException {
        boolean locked = lock.tryLock(time, unit);
        if (!locked) {
            throw new TimeLockException();
        }
    }

    private void safeUnlock(Lock lock) {
        //noinspection CatchMayIgnoreException
        try {
            lock.unlock();
        } catch (IllegalMonitorStateException e) {
        }
    }

}
