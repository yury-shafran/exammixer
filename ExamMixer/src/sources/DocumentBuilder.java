package sources;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class DocumentBuilder {

    public interface ContentProvider {

        void addContent(int index, String contentId, PrintWriter writer) throws Exception;

    }

    private final List<String> literals = new LinkedList<>();
    private final List<String> contentIds = new LinkedList<>();

    public void addLiteral(String... literals) {
        for (String literal : literals) {
            Objects.requireNonNull(literal);
            this.literals.add(literal);
        }
    }

    public void addContent(String... contentIds) {
        for (String contentId : contentIds) {
            this.contentIds.add(contentId);
            literals.add(null);
        }
    }

    public void build(int index, ContentProvider contentProvider, PrintWriter writer) throws Exception {
        Iterator<String> contentIdsIterator = contentIds.iterator();
        for (String literal : literals) {
            if (Objects.isNull(literal)) {
                String contentId = contentIdsIterator.next();
                contentProvider.addContent(index, contentId, writer);
            } else {
                writer.println(literal);
            }
        }
    }

}
