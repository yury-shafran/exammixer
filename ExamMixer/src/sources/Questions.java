package sources;

import java.util.*;

public class Questions {

    // region Inner classes

    public static class Option {

        private final List<String> images = new ArrayList<>(2);
        private final List<String> text = new ArrayList<>(2);

        public void addText(String text) {
            this.text.add(text);
        }

        public void addText(List<String> text) {
            this.text.addAll(text);
        }

        public void addImage(String image) {
            this.images.add(image);
        }

        public void addImages(List<String> images) {
            this.images.addAll(images);
        }

        public List<String> getText() {
            return Collections.unmodifiableList(text);
        }

        public List<String> getImages() {
            return Collections.unmodifiableList(images);
        }

    }

    public static class Question {

        private final Map<String, Option> options = new HashMap<>(16);

        public void addText(String optionId, String text) {
            options.putIfAbsent(optionId, new Option());
            options.get(optionId).addText(text);
        }

        public void addText(String optionId, List<String> text) {
            options.putIfAbsent(optionId, new Option());
            options.get(optionId).addText(text);
        }

        public void addImage(String optionId, String image) {
            options.putIfAbsent(optionId, new Option());
            options.get(optionId).addImage(image);
        }

        public void addImages(String optionId, List<String> images) {
            options.putIfAbsent(optionId, new Option());
            options.get(optionId).addImages(images);
        }

        public Set<String> getOptionsIds(String defaultOptionId) {
            Set<String> optionsIds = new HashSet<>(options.keySet());
            if (optionsIds.size() > 1) {
                optionsIds.remove(defaultOptionId);
            }
            return Collections.unmodifiableSet(optionsIds);
        }

        public List<String> getText(String optionId, String defaultOptionId) throws OptionNotFoundException {
            List<String> text;
            if (optionId.equals(defaultOptionId) && options.containsKey(optionId)) {
                text = options.get(optionId).getText();
            } else if (options.containsKey(defaultOptionId) || options.containsKey(optionId)) {
                text = new ArrayList<>(2);
                String[] ids = {defaultOptionId, optionId};
                for (String id : ids) {
                    if (options.containsKey(id)) {
                        text.addAll(options.get(id).getText());
                    }
                }
            } else {
                throw new OptionNotFoundException(optionId);
            }
            return text;
        }

        public List<String> getImages(String optionId, String defaultOptionId) {
            List<String> images;
            if (optionId.equals(defaultOptionId) && options.containsKey(optionId)) {
                images = options.get(optionId).getImages();
            } else {
                images = new ArrayList<>(2);
                String[] ids = {defaultOptionId, optionId};
                for (String id : ids) {
                    if (options.containsKey(id)) {
                        images.addAll(options.get(id).getImages());
                    }
                }
            }
            return images;
        }

    }

    // endregion Inner classes

    // region Exceptions

    public static class IdNotFoundException extends Exception {

        private final String id;

        public IdNotFoundException(String id) {
            this.id = id;
        }

        public IdNotFoundException(String id, Throwable cause) {
            super(cause);
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public static class OptionNotFoundException extends IdNotFoundException {

        public OptionNotFoundException(String id) {
            super(id);
        }

    }

    public static class QuestionNotFoundException extends IdNotFoundException {

        public QuestionNotFoundException(String id) {
            super(id);
        }

        public QuestionNotFoundException(String id, Throwable cause) {
            super(id, cause);
        }

    }

    // endregion Exceptions

    private String defaultOptionId = "Default";
    private Map<String, Question> questions = new HashMap<>();

    public void setDefaultOptionId(String defaultOptionId) {
        this.defaultOptionId = defaultOptionId;
    }

    public void clear() {
        questions.clear();
    }

    public void addOptionText(String questionId, String optionId, String text) {
        questions.putIfAbsent(questionId, new Question());
        questions.get(questionId).addText(optionId, text);
    }

    public void addOptionText(String questionId, String optionId, List<String> text) {
        questions.putIfAbsent(questionId, new Question());
        questions.get(questionId).addText(optionId, text);
    }

    public void addOptionImage(String questionId, String optionId, String image) {
        questions.putIfAbsent(questionId, new Question());
        questions.get(questionId).addImage(optionId, image);
    }

    public void addOptionImages(String questionId, String optionId, List<String> images) {
        questions.putIfAbsent(questionId, new Question());
        questions.get(questionId).addImages(optionId, images);
    }

    public Set<String> getQuestionsIds() {
        return Collections.unmodifiableSet(questions.keySet());
    }

    public Set<String> getOptionsIds(String questionId) throws QuestionNotFoundException {
        if (!questions.containsKey(questionId)) {
            throw new QuestionNotFoundException(questionId);
        }
        return questions.get(questionId).getOptionsIds(defaultOptionId);
    }

    public List<String> getText(String questionId, String optionId) throws QuestionNotFoundException {
        if (!questions.containsKey(questionId)) {
            throw new QuestionNotFoundException(questionId);
        }
        try {
            return questions.get(questionId).getText(optionId, defaultOptionId);
        } catch (OptionNotFoundException e) {
            throw new QuestionNotFoundException(questionId, e);
        }
    }

    public List<String> getImages(String questionId, String optionId) throws QuestionNotFoundException {
        if (!questions.containsKey(questionId)) {
            throw new QuestionNotFoundException(questionId);
        }
        return questions.get(questionId).getImages(optionId, defaultOptionId);
    }

}
