package sources;

import java.util.HashMap;
import java.util.Map;

public class Templates {

    private final Map<String, DocumentBuilder> builders = new HashMap<>();

    public void clear() {
        builders.clear();
    }

    public void add(String templateId, String line, boolean literal) {
        builders.putIfAbsent(templateId, new DocumentBuilder());
        if (literal) {
            builders.get(templateId).addLiteral(line);
        } else {
            builders.get(templateId).addContent(line);
        }
    }

    public DocumentBuilder get(String templateId) {
        return builders.get(templateId);
    }

}