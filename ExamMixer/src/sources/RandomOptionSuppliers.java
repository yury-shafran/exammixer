package sources;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RandomOptionSuppliers<Q, V> {

    private final Map<Q, RandomOptionSupplier<V>> suppliers = new HashMap<>();

    private RandomOptionSuppliers() {

    }

    public static <Q, V> RandomOptionSuppliers<Q, V>.Builder builder() {
        return new RandomOptionSuppliers<Q, V>().new Builder();
    }

    public boolean contains(Q questionId) {
        return suppliers.containsKey(questionId);
    }

    public V get(Q questionId) {
        return suppliers.get(questionId).get();
    }

    public class Builder {

        @SuppressWarnings("UnusedReturnValue")
        public RandomOptionSuppliers<Q, V>.Builder add(Q questionId, Collection<V> optionsIds) {
            suppliers.put(questionId, new RandomOptionSupplier<>(optionsIds));
            return this;
        }

        public RandomOptionSuppliers<Q, V> build() {
            return RandomOptionSuppliers.this;
        }

    }

}
