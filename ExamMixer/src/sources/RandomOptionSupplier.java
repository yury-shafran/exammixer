package sources;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

public class RandomOptionSupplier<T> implements Supplier<T> {

    private final Random random = ThreadLocalRandom.current();
    private List<T> variants;
    private int index;

    public RandomOptionSupplier(Collection<T> collection) {
        if (collection.size() == 0) {
            throw new IllegalArgumentException();
        }
        variants = new ArrayList<>(collection);
        Collections.shuffle(variants, random);
        index = -1;
    }

    @Override
    public T get() {
        index++;
        if (index == variants.size()) {
            index = 0;
            Collections.shuffle(variants, random);
        }
        return variants.get(index);
    }

}
