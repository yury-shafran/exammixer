package resources;

public class LabelProvider {

    private static final ResourceProvider provider = new ResourceProvider("resources.Labels");

    public static String get(String key, String defaultValue) {
        return provider.getValue(key, defaultValue);
    }

    public static String getSucceeded() {
        return get("Succeeded", "Succeeded");
    }

    public static String getInterruptedException() {
        return get("InterruptedException", "The task was interrupted.");
    }

    public static String getTimeLockException() {
        return get("TimeLockException", "Too long delay. Restart the application.");
    }

    public static String getGenerationResult(String filepath, String message) {
        return String.format(
                LabelProvider.get("GenerationResult", "Generation:\\n%s\\n%s"), filepath, message);
    }

    public static String getLoadTemplateResult(String filepath, String message) {
        return String.format(
                LabelProvider.get("LoadTemplateResult", "Template loading:\\n%s\\n%s"), filepath, message);
    }

    public static String getLoadTextResult(String filepath, String message) {
        return String.format(
                LabelProvider.get("LoadTextResult", "Text loading:\\n%s\\n%s"), filepath, message);
    }

    public static String getLoadImageResult(String filepath, String message) {
        return String.format(
                LabelProvider.get("LoadImageResult", "Image file processing:\\n%s\\n%s"), filepath, message);
    }

    public static String getClearResult(String message) {
        return String.format(
                LabelProvider.get("ClearResult", "Clear:\\n%s"), message);
    }

}
