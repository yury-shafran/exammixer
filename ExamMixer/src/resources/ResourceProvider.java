package resources;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceProvider {

    private final ResourceBundle bundle;

    public ResourceProvider(String resourceBundle, Locale locale) {
        bundle = ResourceBundle.getBundle(resourceBundle, locale);
    }

    public ResourceProvider(String resourceBundle) {
        this(resourceBundle, Locale.getDefault());
    }

    public static String getValue(String resourceBundle, Locale locale, String key, String defaultValue) {
        return getValue(ResourceBundle.getBundle(resourceBundle, locale), key, defaultValue);
    }

    private static String getValue(ResourceBundle bundle, String key, String defaultValue) {
        return bundle.containsKey(key) ? bundle.getString(key) : defaultValue;
    }

    public String getValue(String key, String defaultValue) {
        return getValue(bundle, key, defaultValue);
    }

}
